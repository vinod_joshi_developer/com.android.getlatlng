/**
 *
 */
package ncrb.nic.in.getlatlng.utils;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


/**
 * Class to hold all application utility methods
 *
 * @author Ultron
 */
//@SuppressWarnings("deprecation")
public class Utils {

    private static final String TAG = "Utils";

    /**
     * Constructor to prevent instantiation.
     */
    private Utils() {
        // Nothing to do
    }

    /**
     * Method to verify if network connection available and connecting.
     *
     * @param context
     * @return true if connecting successfully.
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr != null && conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        }
        //Toast.makeText(context, Constants.NO_NET_MSG, Toast.LENGTH_LONG).show();
        return false;
    }

    /**
     * @param
     * @return
     */

    public static String convertToDateFormat(Context context, String new_format_value
            , String existing_format_value, String date_value) {

        Date eDDte;
        try {
            // @Logic : convert string into date value - specially for MySQL
            SimpleDateFormat new_format = new SimpleDateFormat(new_format_value);
            SimpleDateFormat existing_format = new SimpleDateFormat(existing_format_value);
            eDDte = existing_format.parse(date_value);
            return new_format.format(eDDte);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }// end showToastMsg

    /**
     * @param
     * @return
     */

    public static Date convertStrToDate(Context context, String format, String date_value) {

        // @Logic : convert string into date value - specially for MySQL
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        try {
            Date date_final = formatter.parse(date_value);
            return  date_final;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();

    }// end showToastMsg

    /**
     * @param
     * @return
     */

    public static void showToastMsg(Context context, String text, int toastDuration) {
        Toast.makeText(context, text, toastDuration).show();
    }// end showToastMsg



    /**
     * Get current lat and long
     */
    public static HashMap<String, String> getCurrentLatLng(Context context, AppLocationService appLocationService) {

        HashMap<String, String> params = new HashMap<String, String>();

        Location nwLocation = appLocationService
                .getLocation(LocationManager.NETWORK_PROVIDER);

        if (nwLocation != null) {
            double latitude = nwLocation.getLatitude();
            double longitude = nwLocation.getLongitude();
            params.put("lat", "" + latitude);
            params.put("lng", "" + longitude);
            params.put("network_gps_enabled", "true");
        } else {
            params.put("network_gps_enabled", "false");
        }

        return params;

    }// end get Current lat lng

    // find address by latitude and longitude
    public static String getAddressByLatLng(Context context, double LATITUDE, double LONGITUDE) {

        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("Current loc address", "" + strReturnedAddress.toString());
            } else {
                Log.w("Current loc address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("Current loc address", "Canont get Address!");
        }
        return strAdd;
    }

    /**
     * @
     */

    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target.length()!=10) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    /**
     * @
     */
    public static int getSpinnerIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                index = i;
                break;
            }// end if
        }// end for
        System.out.println("returned string " + myString);
        System.out.println("returned index " + index);
        return index;
    }// end get spinner index

    /**
     * @
     */

    public static void showAlert(Context context , String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }//  end show alert




}// end main utils class