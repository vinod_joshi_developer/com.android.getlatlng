package ncrb.nic.in.getlatlng.services;

import ncrb.nic.in.getlatlng.services_params.SubmitFormModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Lenovo on 0011 11-05-2016.
 */
public interface APIInterface {

    @Multipart
    @POST("files/upload")
    Call<SubmitFormModel> upload(@Part("description") RequestBody description
            , @Part MultipartBody.Part file
            , @Part("STATE_CD") RequestBody STATE_CD
            , @Part("DISTRICT_CD") RequestBody DISTRICT_CD
            , @Part("PS_CD") RequestBody PS_CD
            , @Part("SHO_FULL_NAME") RequestBody SHO_FULL_NAME
            , @Part("MOBILE_1") RequestBody MOBILE_1
            , @Part("LAT") RequestBody LAT
            , @Part("LNG") RequestBody LNG
            , @Part("OFFICE_VS_RANK_CD") RequestBody OFFICE_VS_RANK_CD
            , @Part("ADDRESS_LINE_1") RequestBody ADDRESS_LINE_1
            , @Part("ADDRESS_LINE_2") RequestBody ADDRESS_LINE_2
            , @Part("ADDRESS_LINE_3") RequestBody ADDRESS_LINE_3
            , @Part("VILLAGE") RequestBody VILLAGE
            , @Part("TEHSIL") RequestBody TEHSIL
            , @Part("PINCODE") RequestBody PINCODE);

    @Multipart
    @POST("mail")
    Call<SubmitFormModel> sendMail(@Part("description") RequestBody description
            , @Part MultipartBody.Part file
            , @Part("STATE_CD") RequestBody STATE_CD
            , @Part("DISTRICT_CD") RequestBody DISTRICT_CD
            , @Part("PS_CD") RequestBody PS_CD
            , @Part("SHO_FULL_NAME") RequestBody SHO_FULL_NAME
            , @Part("MOBILE_1") RequestBody MOBILE_1
            , @Part("LAT") RequestBody LAT
            , @Part("LNG") RequestBody LNG
            , @Part("OFFICE_VS_RANK_CD") RequestBody OFFICE_VS_RANK_CD
            , @Part("ADDRESS_LINE_1") RequestBody ADDRESS_LINE_1
            , @Part("ADDRESS_LINE_2") RequestBody ADDRESS_LINE_2
            , @Part("ADDRESS_LINE_3") RequestBody ADDRESS_LINE_3
            , @Part("VILLAGE") RequestBody VILLAGE
            , @Part("TEHSIL") RequestBody TEHSIL
            , @Part("PINCODE") RequestBody PINCODE);

}// end api interface
