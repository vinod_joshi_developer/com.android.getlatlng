package ncrb.nic.in.getlatlng.services_params;

/**
 * Created by sez1 on 24/11/15.
 */
public class SubmitFormModel {

    private int statusCode;
    private String status;
    private ResponseData responseData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public ResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

    public class ResponseData {

        private String message;
        public Data data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }
    }

    public class Data {

        private int application_id;

        public int getApplication_id() {
            return application_id;
        }
        public void setApplication_id(int application_id) {
            this.application_id = application_id;
        }
    }
}
