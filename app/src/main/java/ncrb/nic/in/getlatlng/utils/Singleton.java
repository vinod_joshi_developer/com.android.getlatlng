package ncrb.nic.in.getlatlng.utils;

import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 13-04-2016.
 */
public class Singleton {

    private static Singleton singleton = new Singleton( );

    public String state_cd ;
    public String district_cd 	;
    public String ps_code  ;
    public String office_vs_rank_cd ;
    public String office_vs_rank_name ;
    public String incident_from  ;
    public String incident_to  ;
    public String complainant_name  ;
    public String remarks  ;
    public String complaint_desc  ;
    public String mobile_1 ;
    public String mobile_2 ;
    public String full_name ;
    public String add_house_no ;
    public String add_street ;
    public String add_colony ;
    public String add_village ;
    public String add_tehsil ;
    public String add_pincode;
    public String success_message;
    public String nationality_cd;
    public String complaint_nature_cd;


    public GoogleApiClient mGoogleApiClient;
    public List<String> ps_stations_with_km = new ArrayList<String>();
    public String nearby_state ;
    public String nearby_district ;
    public String nearby_ps ;
    public boolean know_your_ps = true;


    /* A private Constructor prevents any other
     * class from instantiating.
     */
    private Singleton(){ }

    /* Static 'instance' method */
    public static Singleton getInstance( ) {
        return singleton;
    }
    /* Other methods protected by singleton-ness */
    protected static void demoMethod( ) {
        System.out.println("demoMethod for singleton");
    }

}// end main singleton
