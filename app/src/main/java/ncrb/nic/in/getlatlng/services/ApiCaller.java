package ncrb.nic.in.getlatlng.services;

import java.util.HashMap;

import ncrb.nic.in.getlatlng.services_params.SubmitFormModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by sez1 on 18/11/15.
 */
public interface ApiCaller {

    @FormUrlEncoded
    @POST("/rest/create_complaint")
    public void complaintRegistration(@FieldMap HashMap<String, String> arr,
                               retrofit2.Callback<SubmitFormModel> callback);

    @FormUrlEncoded
    @POST("/rest/nearby_location")
    public Call<ResponseBody> getNearByPoliceStation(@FieldMap HashMap<String, String> arr);
}// end api caller
