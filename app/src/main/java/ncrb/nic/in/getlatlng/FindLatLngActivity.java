package ncrb.nic.in.getlatlng;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import ncrb.nic.in.getlatlng.utils.AppLocationService;
import ncrb.nic.in.getlatlng.utils.Singleton;
import ncrb.nic.in.getlatlng.utils.Utils;

public class FindLatLngActivity extends AppCompatActivity {

    AppLocationService appLocationService;
    HashMap<String, String> latlng = new HashMap<String, String>();
    Singleton singleton = Singleton.getInstance();
    EditText et_lat;
    EditText et_lng;
    TextView tv_address;
    private static Button trackingButton;
    private static Button btn_lat_lng_on_map;
    private static Button btn_validate_lat_lng;
    public ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_lat_lng);

        Toolbar toolbar = (Toolbar) findViewById(ncrb.nic.in.getlatlng.R.id.toolbar);
        setSupportActionBar(toolbar);

        et_lat = (EditText) findViewById(R.id.id_lat);
        et_lng = (EditText) findViewById(R.id.id_lng);
        tv_address = (TextView) findViewById(R.id.tv_address);
        trackingButton = (Button) findViewById(R.id.btn_find_lat_lng);
        btn_lat_lng_on_map = (Button) findViewById(R.id.btn_lat_lng_on_map);
        btn_validate_lat_lng = (Button) findViewById(R.id.btn_validate_lat_lng);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");


        // remove keyboard on load
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        appLocationService = new AppLocationService(FindLatLngActivity.this);

        getWindow().getAttributes().windowAnimations = R.style.WindowAnimationTransition;


        // validate lat long on Google map
        btn_validate_lat_lng.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                validateLatLng();
            }
        });

        // view map on Google app
        btn_lat_lng_on_map.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                findLatLngOnMap();
            }
        });

    }// end on create


    public void findLatLng(View V) {


        FindLatLngActivity.this.mProgressDialog.show();

        latlng = Utils.getCurrentLatLng(FindLatLngActivity.this, appLocationService);
        //network_gps_enabled = true;
        if (latlng.get("network_gps_enabled").equals("true")) {

            setTrackingButtonState();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    // todo remove this
                    //latlng.put("lat", "28.566008"); //latlng.put("lng", "77.176743");
                    System.out.println("latlng " + latlng);
                    et_lat.setText(latlng.get("lat"));
                    et_lng.setText(latlng.get("lng"));
                    String address = Utils.getAddressByLatLng(FindLatLngActivity.this, Double.parseDouble(latlng.get("lat").trim())
                            , Double.parseDouble(latlng.get("lng").trim()));
                    tv_address.setText(address);

                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();

                }
            }, 3000); // end splash handler

        } else {
            Utils.showAlert(FindLatLngActivity.this, "Either location permission not given to app or GPS location not available. " +
                    "\n\nPlease allow location permission. i.e. Settings > App > Permissions.");
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();

        }


    }// end find lat lng

    @Override
    public void onResume() {
        super.onResume();

        et_lat.setText("");
        et_lng.setText("");
        tv_address.setText("");

        setTrackingButtonState();

    }// on Resume

    /**
     * validate form address
     */
    public void validateLatLng() {

        latlng = Utils.getCurrentLatLng(FindLatLngActivity.this, appLocationService);
        //network_gps_enabled = true;
        if (latlng.get("network_gps_enabled").equals("true")) {

            String url = "http://maps.google.com/maps?q=" + latlng.get("lat") + "," + latlng.get("lng");
            startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url)));

        } else {
            Utils.showAlert(FindLatLngActivity.this, "GPS location not available. " +
                    "Please check location services to access services of this App.");
        }

    }  // end validate

    private void setTrackingButtonState() {

        latlng = Utils.getCurrentLatLng(FindLatLngActivity.this, appLocationService);
        if (latlng.get("network_gps_enabled").equals("true")) {
            trackingButton.setBackgroundResource(R.drawable.green_tracking_button);
            trackingButton.setTextColor(Color.BLACK);
            trackingButton.setText(R.string.get_lat_lng_on);
        } else {
            trackingButton.setBackgroundResource(R.drawable.red_tracking_button);
            trackingButton.setTextColor(Color.WHITE);
            trackingButton.setText(R.string.get_lat_lng_off);
        }
    }// end set Tracking Button State

    /**
     * @
     */

    public void findLatLngOnMap() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Long press on map to get latitude and longitude on top of the screen.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        FindLatLngActivity.this.validateLatLng();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }//  end show alert

}// end main complaint reg one activity
